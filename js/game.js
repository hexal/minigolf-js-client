const canvas = document.querySelector('#map-canvas');
const ctx = canvas.getContext('2d');

const ballCanvas = document.querySelector('#ball-canvas');
const ballCtx = ballCanvas.getContext('2d');

const tileset = new Image();
tileset.src = 'img/minigolf-tiles.png';
tileset.onload = draw;
const ballImage = new Image();
ballImage.src = 'img/minigolf-ball.png';
const ballSpeed = 5;
const mapData = [17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 20, 1, 1, 1, 20, 1, 1, 20, 20, 1, 1, 20, 1, 20, 1, 20, 1, 20, 1, 20, 1, 1, 20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 20, 20, 1, 20, 20, 1, 20, 1, 1, 20, 1, 20, 1, 20, 20, 1, 1, 20, 20, 1, 1, 20, 1, 20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 20, 1, 20, 1, 20, 1, 20, 1, 1, 20, 1, 20, 1, 20, 20, 1, 1, 20, 20, 1, 1, 20, 20, 20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 20, 1, 1, 1, 20, 1, 1, 20, 20, 1, 1, 20, 1, 20, 1, 20, 1, 20, 1, 20, 1, 20, 1, 20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17];
let tileSize = 15;
const tilesetColumn = 4;
const tilesetRow = 6;
let tilesetIndex = 0;
let tilesetX = 0;
let tilesetY = 0;
const mapRows = 25;
const mapCols = 49;
const mapHeight = mapRows * tileSize;
const mapWidth = mapCols * tileSize;
const startingPointX = 15;
const startingPointY = 15;
let collisionArray = [];

function sleep(ms) {
   return new Promise(resolve => setTimeout(resolve, ms));
}

let Ball = class {
   constructor(x, y, dx, dy, startX, startY, targetX, targetY, speed, collisionX, collisionY) {
      this.x = x;
      this.y = y;
      this.dx = x;
      this.dy = y;
      this.startX = startX;
      this.startY = startX;
      this.targetX = targetX;
      this.targetY = targetY;
      this.speed = speed;
      this.collisionX = collisionX;
      this.collisionY = collisionY;
   }

   render() {
      ballCtx.clearRect(0, 0, ballCanvas.width, ballCanvas.height);
      // Set the center of the ball directly into the coordinates. Nasty, lazy hack, idk.
      ballCtx.drawImage(ballImage, this.x - (tileSize / 2), this.y - (tileSize / 2));
   }

   shoot() {
      if(this.didCollide) {
         console.log('did collide');
      }
      else {
         console.log('did not collide');
      }
      this.x = this.x - this.dx;
      this.y = this.y - this.dy;
      this.render();
      requestAnimationFrame(() => ball.shoot());
   }

   setCollisionPoint() {
      let x = this.x;
      let y = this.y;
      let dx = this.dx;
      let dy = this.dy;
      let didCollide = false;
      while(!didCollide) {
         collisionArray.forEach(collisionPoint => {
            if(
               // TODO: Copypasta from MDN, implement yourself, this one is collision between 2 squares
               // collisionPoint.x < x + tileSize &&
               // collisionPoint.x + tileSize > x &&
               // collisionPoint.y < y + tileSize &&
               // tileSize + collisionPoint.y > y
            ) {
               didCollide = true;
               this.collisionX = x;
               this.collisionY = y;
            }
         });

      }
   }
}
let ball = new Ball(startingPointX * tileSize, startingPointY * tileSize, 0, 0, startingPointX * tileSize, startingPointY * tileSize, 0, 0, ballSpeed);

function draw() {
   for(let column = 0; column < mapHeight; column += tileSize) {
      for(let row = 0; row < mapWidth; row += tileSize) {
         let tileValue = mapData[tilesetIndex];
         if(tileValue != 0) {
            // in tilemaps, 0 is used as an empty value that will be ignored, thus -1
            tileValue -= 1;
            tilesetY = Math.floor(tileValue / tilesetColumn) * tileSize;
            // TODO: There are more tile types that cause collition
            if(tileValue === 17 - 1) {
               collisionArray.push({x: column, y: row});
            }
            tilesetX = (tileValue % tilesetColumn) * tileSize;
            ctx.drawImage(tileset, tilesetX, tilesetY, tileSize, tileSize, row, column, tileSize, tileSize);
         }
         tilesetIndex++;
      }
   }
   ball.render();
}

console.log(collisionArray)

function getMousePosition(canvas, evt) {
   const rect = canvas.getBoundingClientRect();
   return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
   };
}

function startShoot(evt) {
   const mousePosition = getMousePosition(ballCanvas, evt);
   ball.targetX = mousePosition.x;
   ball.targetY = mousePosition.y;

   // Calculate deltas with polar coordinates
   let direction = Math.atan2(ball.y - ball.targetY, ball.x - ball.targetX);
   ball.dx = Math.cos(direction) * ball.speed;
   ball.dy = Math.sin(direction) * ball.speed;
   ball.setCollisionPoint();
   requestAnimationFrame(() => ball.shoot(mousePosition));
}


function drawSight(evt) {
   const mousePosition = getMousePosition(ballCanvas, evt);
   ballCtx.clearRect(0, 0, ballCanvas.width, ballCanvas.height);
   ball.render();
   ballCtx.beginPath();
   ballCtx.moveTo(ball.x, ball.y);
   ballCtx.lineTo(mousePosition.x, mousePosition.y);
   ballCtx.stroke();
}

canvas.addEventListener('click', (evt) => startShoot(evt));
canvas.addEventListener('mousemove', (evt) => drawSight(evt));